"""
@author: Martina Casari
"""
import numpy as np
import pandas as pd

import logging

from matplotlib import pyplot as plt
from scipy import stats
from sklearn.impute import KNNImputer


class DataPreprocessing:
    def __init__(self, mith_parameters=None):
        self.df = pd.DataFrame()

        self.preprocessing_params = mith_parameters['preprocessing'] \
            if 'preprocessing' in mith_parameters else \
            {
                'std': 3,
                'k': 5,
                'smoothing_window': '1h'
            }

    def preprocess_data(self, df: pd.DataFrame):
        self.df = df.copy()

        self.remove_statistical_anomalies(deviation=self.preprocessing_params['std'])
        # preprocessing.change_nan_columns_to_zero()
        self.imputing_missing_values(method='knn', k=self.preprocessing_params['k'])

        window = self.preprocessing_params['smoothing_window']

        columns = df.columns[df.columns.str.contains('pm')]
        for column in columns:
            self.one_sided_median_filter(column=column, window_size=window)

        return self.df

    def get_averaged(self, freq="1h", method="ffill"):
        try:
            self.df = self.df.resample(freq).mean()
            self.df.fillna(method=method, inplace=True)
        except Exception:
            logging.error("Frequency not recognized")
            return

        return self.df

    def get_normalized(self, method="z-score"):
        if method == "z-score":
            self.df = (self.df - self.df.mean()) / self.df.std()
        elif method == "min-max":
            self.df = (self.df - self.df.min()) / (self.df.max() - self.df.min())
        else:
            logging.error("Method not recognized")
            return

        return self.df

    def remove_statistical_anomalies(self, deviation=3, columns=None):
        if columns is None:
            columns = self.df.columns

        mask = self.df[columns].gt(self.df[columns].mean() + deviation * self.df[columns].std()) | self.df[columns].lt(
            self.df[columns].mean() - deviation * self.df[columns].std())
        self.df[columns] = self.df[columns].where(mask == False, np.nan)

        return self.df

    def change_nan_columns_to_zero(self):
        for column in self.df.columns:
            if self.df[column].isnull().sum() == len(self.df):
                self.df[column] = self.df[column].fillna(0)

        return self.df

    def get_one_sided_median_filter_threshold(self, column, window_size='2h'):
        serie = self.df[column].copy()

        medians_diff = []
        for i in range(len(serie)):
            i_date = serie.index[i]
            # left = serie [i_date - pd.Timedelta(window_size):i_date]
            left = serie.loc[(serie.index >= i_date - pd.Timedelta(window_size)) & (serie.index <= i_date)]

            medians_diff.append(abs(np.median(left) - serie.iloc[i]))

        return np.percentile(medians_diff, 75)

    def one_sided_median_filter(self, column, window_size='2h'):

        threshold = self.get_one_sided_median_filter_threshold(column=column, window_size=window_size)

        serie = self.df[column].copy()

        for i in range(len(serie)):
            i_date = serie.index[i]
            start_date = i_date - pd.Timedelta(window_size)
            left = serie.loc[(serie.index >= start_date) & (serie.index <= i_date)]

            # left = serie[start_date:i_date]
            if len(left) == 0:
                continue

            median = np.median(left)

            if serie.iloc[i] > median + threshold or serie.iloc[i] < median - threshold:
                self.df.loc[i_date, column] = median

    def imputing_missing_values(self, method="mean", k=5):
        if method == "mean":
            self.df = self.df.fillna(self.df.mean())
        elif method == "median":
            self.df = self.df.fillna(self.df.median())
        elif method == "mode":
            self.df = self.df.fillna(self.df.mode())
        elif method == 'knn':
            imputer = KNNImputer(missing_values=np.nan, n_neighbors=k, weights='distance')
            imputed_data = imputer.fit_transform(self.df)
            self.df = pd.DataFrame(imputed_data, columns=self.df.columns, index=self.df.index)
        else:
            logging.error("Method not recognized")
            return

        return self.df

    def get_differenced(self, lag=1):
        self.df = self.df.diff(lag)

        return self.df

    def get_log_transformed(self):
        self.df = np.log(self.df)

        return self.df

    def get_exp_transformed(self):
        self.df = np.exp(self.df)

        return self.df

    def get_inverse_boxcox_transformed(self, lmbda):
        self.df = stats.boxcox(self.df, lmbda=lmbda)

        return self.df

    def get_boxcox_transformed(self):
        self.df = stats.boxcox(self.df)[0]

        return self.df

    def get_rolling_mean(self, window=3):
        self.df = self.df.rolling(window=window).mean()

        return self.df

    def get_rolling_std(self, window=3):
        self.df = self.df.rolling(window=window).std()

        return self.df

    def get_rolling_median(self, window=3):
        self.df = self.df.rolling(window=window).median(numeric_only=True)

        return self.df

    def get_rolling_min(self, window=3):
        self.df = self.df.rolling(window=window).min()

        return self.df

    def get_rolling_max(self, window=3):
        self.df = self.df.rolling(window=window).max()

        return self.df

    def get_rolling_skew(self, window=3):
        self.df = self.df.rolling(window=window).skew()

        return self.df

    def get_rolling_kurt(self, window=3):
        self.df = self.df.rolling(window=window).kurt()

        return self.df

    def get_rolling_quantile(self, window=3, quantile=0.5):
        self.df = self.df.rolling(window=window).quantile(quantile=quantile)

        return self.df

    def get_rolling_cov(self, window=3, other=pd.DataFrame):
        self.df = self.df.rolling(window=window).cov(other=other)

        return self.df

    def get_rolling_corr(self, window=3, other=pd.DataFrame):
        self.df = self.df.rolling(window=window).corr(other=other)

        return self.df

    def get_truncated(self, lower=0.01, upper=0.99):
        self.df = self.df.clip(lower=self.df.quantile(lower), upper=self.df.quantile(upper), axis=1)

        return self.df

    def get_rolling_truncated(self, window=3, lower=0.01, upper=0.99):
        self.df = self.df.rolling(window=window).apply(
            lambda x: x.clip(lower=x.quantile(lower), upper=x.quantile(upper)))

        return self.df

    def get_rolling_truncated_mean(self, window=3, lower=0.01, upper=0.99):
        self.df = self.df.rolling(window=window).apply(
            lambda x: x.clip(lower=x.quantile(lower), upper=x.quantile(upper)).mean())

        return self.df

    def get_preprocessing_impact(self, column):
        df_ = self.original_df[column] != self.df[column]

        return df_.sum() / len(self.original_df[column])

    def get_df(self):
        return self.df
