import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from src.algorithms.growth_function.growth_functions import GrowthFunctionFactory
from src.algorithms.growth_function.growth_functions_optimizer import GrowthFunctionsOptimizer


class CorrectiveProcedure:
    def __init__(self, mith_parameters=None):
        self.mith_parameters = mith_parameters

        self.parameters_range = {}
        self.set_default_parameters_range()

        # self.gf_params = None

        self.operative_window_optimized_params = None

    def set_default_parameters_range(self):
        self.parameters_range = {
            'hanel': {
                'beta': np.arange(0.1, 3.3, 0.1)},
            'combo': {
                # 'alfa': np.arange(0.1, 1, 0.1),
                'alfa': [0.1],  # np.arange(0.1, 0.1, 0.1),
                'beta': np.arange(0.1, 8, 0.05)
            },
            'chakrabarti': {
                'alfa': [0.72],  # np.arange(0.1, 0.1, 0.1),
                'beta': np.arange(0.05, 10, 0.01)
            },
            'richards': {
                'alfa': [-0.72],  # np.arange(-1, -0.1, 0.1),
                'beta': np.arange(-3, -0.1, 0.01)
            },
            'chakcombo': {
                'alfa': [0.6],  # np.arange(0.60, 0.80, 0.1),
                'beta': [0.4],  # np.arange(0.30, 0.50, 0.1),
                'gamma': np.arange(0.01, 5, 0.01)
            }
        }

    def correct_data(self, df: pd.DataFrame, columns_to_correct=None):
        history_size = self.mith_parameters['history_size']
        first_date_to_correct = df.index.min() + pd.Timedelta(history_size)

        df_corrected = pd.DataFrame()
        while first_date_to_correct < df.index.max():
            print(f'\r{first_date_to_correct}/{df.index.max()}', end='', flush=True)

            if self.mith_parameters['mode'] == 'batch_mode':
                last_data_to_correct = first_date_to_correct + pd.Timedelta(self.mith_parameters['batch_size'])
                window_batch = df[(df.index >= first_date_to_correct) & (df.index <= last_data_to_correct)]
            else:
                window_batch = df[df.index >= first_date_to_correct].head(1)

            history_data = df[
                (df.index >= first_date_to_correct - pd.Timedelta(history_size)) & (df.index < first_date_to_correct)]

            if self.mith_parameters['inclusive']:
                operative_window = pd.concat([history_data, window_batch])
            else:
                operative_window = history_data

            if not operative_window.empty:
                if self.mith_parameters['fixed_params']:
                    self.operative_window_optimized_params = self.mith_parameters['fixed_params_values']
                else:
                    self.optimize(df=operative_window)

            if not window_batch.empty:
                corrected_window = self.apply_correction(window_batch, columns_to_correct=columns_to_correct)
                df_corrected = pd.concat([df_corrected, corrected_window])

            if self.mith_parameters['mode'] == 'batch_mode':
                first_date_to_correct = last_data_to_correct + pd.Timedelta('1h')
            else:
                first_date_to_correct = df[df.index >= first_date_to_correct].index[1]

        print('\n')

        return df_corrected

    def apply_correction(self, df, columns_to_correct=None):
        if columns_to_correct is None:
            columns_to_correct = ['pm2p5_x']

        df_ = df.copy()
        for column_name in columns_to_correct:
            gf = GrowthFunctionFactory(self.operative_window_optimized_params)
            df_to_correct = df.copy()
            pm_corrected = gf.apply_growth_function(self.mith_parameters['gf_algo'],
                                                    df_to_correct[column_name],
                                                    df_to_correct['relative_humidity'])

            if self.mith_parameters['apply_over_threshold']:
                current_date_to_correct_over_threshold = df[
                    df['relative_humidity'] >= self.mith_parameters['rh_threshold']]

                if not current_date_to_correct_over_threshold.empty:
                    index = current_date_to_correct_over_threshold.index
                else:
                    index = df.index

                df_.loc[df.index.isin(index), column_name] = pm_corrected[index]
            else:
                df_[column_name] = pm_corrected

        return df_

    def optimize(self, df=None):
        gf_algo = self.mith_parameters['gf_algo']

        gf_optimizer = GrowthFunctionsOptimizer(df=df,
                                                algo=gf_algo,
                                                optimization_method=self.mith_parameters['optimization_method'],
                                                verbose=self.mith_parameters['verbose'])
        gf_optimizer.set_params_ranges(params_names=list(self.parameters_range[gf_algo].keys()),
                                       params_ranges=self.parameters_range[gf_algo])
        gf_optimizer.optimize()

        if self.mith_parameters['show_plot']:
            gf_optimizer.get_figure_of_merit('')
            plt.show()

        self.operative_window_optimized_params = gf_optimizer.get_best_params()
