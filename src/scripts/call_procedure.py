import pandas as pd
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_absolute_percentage_error

import matplotlib.pyplot as plt

from src.preprocessing.data_preprocessing import DataPreprocessing
from src.scripts.procedure import CorrectiveProcedure

from tabulate import tabulate

import os

save_figures = True
save_data_to_file = True

test_name = 'test13_mean_allvaliddata_correctallpm'

method = 'mean'

master_directory = f'../../results/{test_name}'
if not os.path.exists(master_directory):
    # If it doesn't, create the directory and its subfolders
    os.makedirs(master_directory)
    print(f"Directory '{master_directory}' created successfully.")
    os.makedirs(f'../../results/{test_name}/img/raw_data')
    os.makedirs(f'../../results/{test_name}/img/preprocessed_data')
    os.makedirs(f'../../results/{test_name}/img/corrected_data')

    os.makedirs(f'../../results/{test_name}/raw_data/not_resampled')
    os.makedirs(f'../../results/{test_name}/raw_data/1h')

    os.makedirs(f'../../results/{test_name}/preprocessed_data/not_resampled')
    os.makedirs(f'../../results/{test_name}/preprocessed_data/1h')

    os.makedirs(f'../../results/{test_name}/corrected_data/not_resampled')
    os.makedirs(f'../../results/{test_name}/corrected_data/1h')

else:
    print(f"Directory '{master_directory}' already exists.")

results_df = pd.DataFrame(
    columns=['sensor_id', 'step', 'r2_value', 'rmse_value', 'mae_value', 'mape_value', 'min_date', 'max_date'])


def get_hourly_data(data, method='nearest'):
    df_ = data.copy()

    if method == 'nearest':
        df_ = df_.resample('1h').nearest()
        df_['d_m_y_H'] = df_.index.round('1h')

        # get difference in hours between index and d_m_y_H
        df_['diff'] = df_.index - df_['d_m_y_H']
        df_['diff'] = abs(df_['diff'].dt.total_seconds() / 60)

        df_.index = df_.index.round('1h')

        df__ = df_.sort_values('diff').groupby('d_m_y_H').first()
        df__ = df__.drop(columns=['diff'])
    elif method == 'mean':
        df_['d_m_y_H'] = df_.index.round('1h')
        original_dates = df_['d_m_y_H'].unique()
        df__ = df_.resample('1h').mean()

        df__ = df__[df__.index.isin(original_dates)]
        df__.drop(columns=['d_m_y_H'], inplace=True)
        df__.dropna(inplace=True)
    else:
        raise ValueError('Method not implemented')

    return df__


def merge_and_print_performance(df_input, target, column1, column2, date_range=None, step=None, sensor_id=None):
    global results_df

    target_copy = target.copy()
    df_input = df_input[df_input.index.isin(target_copy.index)]
    target_copy = target_copy[target_copy.index.isin(df_input.index)]
    df_input = df_input.merge(target_copy, left_index=True, right_index=True)
    df_input.rename(columns={'pm2p5': 'pm2p5_y'}, inplace=True)

    if date_range:
        date_range_min = date_range['min']
        date_range_max = date_range['max']
        print(f'Cutting data at {date_range_min} - {date_range_max}')
        df_input = df_input[(df_input.index >= date_range_min) & (df_input.index <= date_range_max)]
    else:
        date_range_min = df_input.index.min()
        date_range_max = df_input.index.max()

    r2 = r2_score(df_input[column1], df_input[column2])
    rmse = mean_squared_error(df_input[column1], df_input[column2])
    mae = mean_absolute_error(df_input[column1], df_input[column2])
    mape = mean_absolute_percentage_error(df_input[column1], df_input[column2])

    results_df = pd.concat([results_df,
                            pd.DataFrame({'sensor_id': sensor_id, 'step': step, 'r2_value': r2, 'rmse_value': rmse,
                                          'mae_value': mae, 'mape_value': mape,
                                          'min_date': date_range_min, 'max_date': date_range_max}, index=[0])],
                           ignore_index=True)

    return df_input


def plot_time_series(data, column1, column2, step, sensor_id, date_range=None, save=False):
    if date_range:
        data = data[(data.index >= date_range['min']) & (data.index <= date_range['max'])]

    data[[column1, column2]].plot(figsize=(10, 6))
    plt.xlabel('Date')
    plt.ylabel('PM2.5 mass concentration (µg/m³)')
    plt.title(f'Time Series \n{step} - {sensor_id}')
    # plt.ylim([0, 100])
    plt.xticks(rotation=45, ha='right')
    plt.grid(True, axis='y')

    plt.legend(loc='upper right')

    if save:
        plt.savefig(f'../../results/{test_name}/img/{step}_data/{sensor_id}.png')

    plt.show()


def plot_scatter(data, column1, column2, step, sensor_id, date_range=None, save=False):
    if date_range:
        data = data[(data.index >= date_range['min']) & (data.index <= date_range['max'])]

    plt.figure(figsize=(10, 6))
    plt.scatter(data[column1], data[column2], c=data['relative_humidity'], cmap='viridis')

    plt.colorbar(label='Relative Humidity')
    plt.clim(0, 100)

    plt.xlabel('PM2.5 mass concentration (µg/m³) - ARPA')
    plt.ylabel('PM2.5 mass concentration (µg/m³) - Sensor')
    plt.title(f'Scatter Plot \n{step} - {sensor_id}')
    plt.grid(True, axis='both')

    column1_max = data[column1].max()
    plt.plot([0, column1_max], [0, column1_max], color='black', linestyle='--')

    if save:
        plt.savefig(f'../../results/{test_name}/img/{step}_data/scatter_{sensor_id}.png')

    plt.show()


def save_data(df_not_resampled_, df_1h_, sensor_id, step):
    df_not_resampled = df_not_resampled_.copy()
    df_1h = df_1h_.copy()

    columns = df_not_resampled.columns.tolist()
    df_not_resampled['sensor_id'] = sensor_id
    df_not_resampled = df_not_resampled[['sensor_id'] + columns]

    df_1h['sensor_id'] = sensor_id
    df_1h = df_1h[['sensor_id'] + columns + ['pm2p5_y']]
    df_1h.index.name = 'valid_at'

    df_not_resampled.to_csv(f'../../results/{test_name}/{step}_data/not_resampled/{sensor_id}.csv', index=True,
                            header=True)
    df_1h.to_csv(f'../../results/{test_name}/{step}_data/1h/{sensor_id}.csv', index=True, header=True)


for sensor in ['ari-1727', 'ari-1952', 'ari-1953', 'ari-2049', 'ari-1885', 'ari-2074']:
    df = pd.read_csv(f'../../data/single_sensors/{sensor}.csv', parse_dates=['valid_at'], index_col='valid_at')

    df = df[['sensor_id', 'pm1', 'pm2p5', 'pm4', 'pm10',
             'relative_humidity', 'temperature',
             'pressure', 'wind_speed', 'cloud_coverage']]
    df.rename(columns={'pm2p5': 'pm2p5_x'}, inplace=True)

    target_df = pd.read_csv('../../data/single_sensors/arpa.csv', parse_dates=['valid_at'], index_col='valid_at')

    # set MitH parameters
    mith_parameters = {'gf_algo': 'combo',
                       'fixed_params': False,
                       'parameters_range': {},
                       'preprocess': True,
                       'rh_threshold': 60,
                       'apply_over_threshold': True,
                       'measure': 'r2_value',
                       'mode': 'batch_mode',
                       'batch_size': '1d',
                       'history_size': '2d',
                       'preprocessing': {'std': 3,
                                         'k': 5,
                                         'smoothing_window': '2h'},
                       'show_plot': False,
                       'plot_intermediate_results': False,
                       'inclusive': True,
                       'optimization_method': 'min-correlation',
                       'corrected_pm_path': f'../../results/{test_name}/corrected_data/',
                       'target_column': 'pm2p5_y',
                       'verbose': False}

    sensor_id = df['sensor_id'].unique()[0]
    print(f'- Calling MitH for sensor {sensor_id} -')

    df = df.drop(columns=['sensor_id'])
    # df = df[(df.index >= '2022-03-01') & (df.index <= '2022-05-01')]

    sensor_data = df.copy()

    # RAW DATA
    sensor_data_mean = get_hourly_data(sensor_data, method=method)

    # date_range = (
    #     {'min': '2022-03-01', 'max': '2022-05-08'}
    #     if sensor_id in ['ari-1727', 'ari-1952', 'ari-1953']
    #     else {'min': '2022-10-01', 'max': '2022-12-01'}
    #     if sensor_id == 'ari-2049'
    #     else {'min': '2022-10-01', 'max': '2022-12-01'}
    #     if sensor_id == 'ari-1885'
    #     else {'min': '2023-01-01', 'max': '2023-03-01'}
    # )

    date_range = (
        {'min': '2021-06-27', 'max': '2022-05-08'}
        if sensor_id == 'ari-1727'
        else {'min': '2022-02-01', 'max': '2022-05-08'}
        if sensor_id == 'ari-1952'
        else {'min': '2022-02-01', 'max': '2022-05-08'}  # 2022-07-10
        if sensor_id == 'ari-1953'
        else {'min': '2022-08-24', 'max': '2023-06-22'}
        if sensor_id == 'ari-2049'
        else {'min': '2022-09-25', 'max': '2023-05-02'}
        if sensor_id == 'ari-1885'
        else {'min': '2022-12-11', 'max': '2023-07-08'}
    )

    # date_range = None

    sensor_data_mean = merge_and_print_performance(sensor_data_mean, target_df, 'pm2p5_y', 'pm2p5_x',
                                                   date_range=date_range, step='raw', sensor_id=sensor_id)

    if save_data_to_file:
        save_data(sensor_data, sensor_data_mean, sensor_id, 'raw')

    plot_time_series(sensor_data_mean, 'pm2p5_y', 'pm2p5_x', 'raw', sensor_id, date_range=date_range, save=save_figures)
    plot_scatter(sensor_data_mean, 'pm2p5_y', 'pm2p5_x', 'raw', sensor_id, date_range=date_range, save=save_figures)

    # PREPROCESSED DATA
    preprocessing = DataPreprocessing(mith_parameters)
    df_preprocessed = preprocessing.preprocess_data(sensor_data)

    df_preprocessed_mean = get_hourly_data(df_preprocessed, method=method)

    df_preprocessed_mean = merge_and_print_performance(df_preprocessed_mean, target_df, 'pm2p5_y', 'pm2p5_x',
                                                       date_range=date_range, step='preprocessed', sensor_id=sensor_id)

    if save_data_to_file:
        save_data(df_preprocessed, df_preprocessed_mean, sensor_id, 'preprocessed')

    plot_time_series(df_preprocessed_mean, 'pm2p5_y', 'pm2p5_x', 'preprocessed', sensor_id, save=save_figures)
    plot_scatter(df_preprocessed_mean, 'pm2p5_y', 'pm2p5_x', 'preprocessed', sensor_id, save=save_figures)

    # CORRECTED DATA
    procedure = CorrectiveProcedure(mith_parameters)
    df_corrected = procedure.correct_data(df_preprocessed, columns_to_correct=['pm1', 'pm2p5_x', 'pm4', 'pm10'])

    df_corrected_mean = get_hourly_data(df_corrected, method=method)

    df_corrected_mean = merge_and_print_performance(df_corrected_mean, target_df, 'pm2p5_y', 'pm2p5_x',
                                                    date_range=date_range, step='corrected', sensor_id=sensor_id)

    if save_data_to_file:
        save_data(df_corrected, df_corrected_mean, sensor_id, 'corrected')

    plot_time_series(df_corrected_mean, 'pm2p5_y', 'pm2p5_x', 'corrected', sensor_id, save=save_figures)
    plot_scatter(df_corrected_mean, 'pm2p5_y', 'pm2p5_x', 'corrected', sensor_id, save=save_figures)

print(tabulate(results_df, headers='keys', tablefmt='psql'))

results_df.to_csv(f'../../results/{test_name}/results.csv', index=False, header=True)
mith_parameters_df = pd.DataFrame(mith_parameters, index=[0])
mith_parameters_df.to_csv(f'../../results/{test_name}/mith_parameters.csv', index=False, header=True)
