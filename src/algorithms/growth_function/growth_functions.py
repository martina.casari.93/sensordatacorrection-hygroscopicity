import math

import numpy as np


class HanelGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.hanel
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.hanel(rh)

    def hanel(self, rh):
        beta = self.growth_function_parameters['beta']

        return 1 / pow(1 - rh, beta)


class ComboGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.combo
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.combo(rh)

    def combo(self, rh):
        alfa = self.growth_function_parameters['alfa']
        beta = self.growth_function_parameters['beta']

        return 1 + (alfa * (pow(rh, 2)) / pow(1 - rh, beta))


class SonejaGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.soneja
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.soneja(rh)

    def soneja(self, rh):
        alfa = self.growth_function_parameters['alfa']

        return 1 + ((alfa * pow(rh, 2)) / (1 - rh))


class ChakrabartiGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.chakrabarti
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.chakrabarti(rh)

    def chakrabarti(self, rh):
        alfa = self.growth_function_parameters['alfa']
        beta = self.growth_function_parameters['beta']

        return alfa + ((beta * pow(rh, 2)) / (1 - rh))


class ChakComboGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.chakcombo
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.chakcombo(rh)

    def chakcombo(self, rh):
        alfa = self.growth_function_parameters['alfa']
        beta = self.growth_function_parameters['beta']
        gamma = self.growth_function_parameters['gamma']

        return alfa + (beta * (pow(rh, 2)) / pow(1 - rh, gamma))


class RichardsGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.richards
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.richards(rh)

    def richards(self, rh):
        alfa = self.growth_function_parameters['alfa']
        beta = self.growth_function_parameters['beta']

        return (alfa + beta * np.log(1 - rh)).apply(np.exp)


class KKholerGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.kkohler
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.kkohler(rh)

    def kkohler(self, rh):
        k = self.growth_function_parameters['k']

        return pow(1 + (k * (rh / (100 - rh))), 1 / 3)


class SkupinGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.skupin
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.skupin(rh)

    def skupin(self, rh, threshold):
        alfa = self.growth_function_parameters['alfa']
        beta = self.growth_function_parameters['beta']

        rh[rh >= threshold] = alfa / pow(1 - rh[rh >= threshold], beta)
        rh[rh < threshold] = 1 / pow(1 - rh[rh < threshold], (beta - math.log(alfa) / math.log(0.3)))

        return rh  # TODO: must return a function


class ExponentialGrowthFunction:
    def __init__(self, growth_function_parameters):
        self.growth_function = self.exponential
        self.growth_function_parameters = growth_function_parameters

    def __call__(self, rh):
        return self.growth_function(rh)

    def exponential(self, rh):
        exp = self.growth_function_parameters['exp']

        return pow((1 - rh), exp)


class GrowthFunctionFactory:
    growth_function_parameters: object

    def __init__(self, growth_function_parameters):
        self.growth_function_parameters = growth_function_parameters

    def get_growth_function(self, growth_function_name):
        if growth_function_name in ['Hanel', 'hanel']:
            return HanelGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Combo', 'combo']:
            return ComboGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Soneja', 'soneja']:
            return SonejaGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Chakrabarti', 'chakrabarti']:
            return ChakrabartiGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['chakcombo']:
            return ChakComboGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Richards', 'richards']:
            return RichardsGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['KKohler', 'kkohler']:
            return KKholerGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Skupin', 'skupin']:
            return SkupinGrowthFunction(self.growth_function_parameters)
        elif growth_function_name in ['Exponential', 'exponential', 'exp']:
            return ExponentialGrowthFunction(self.growth_function_parameters)
        else:
            raise ValueError('Growth function not found.')

    @staticmethod
    def update_rh_range(rh):
        if max(rh) > 1:
            return rh / 100

    def apply_growth_function(self, growth_function_name, pm, rh):
        rh = self.update_rh_range(rh)
        return pm / self.get_growth_function(growth_function_name)(rh)
