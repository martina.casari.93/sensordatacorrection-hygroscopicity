# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 10:28:19 2022

@author: marti
"""
import itertools
import math
from csv import writer

import pandas as pd

from tslearn.metrics import dtw

from src.algorithms.growth_function.figure_of_merit import FigureOfMerit
from src.algorithms.growth_function.growth_functions import GrowthFunctionFactory


class GrowthFunctionsOptimizer:
    def __init__(self, file_path=None, df=None, rh_column='relative_humidity', pm_column='pm2p5_x',
                 optimization_method=None, algo=None, threshold=0.7,
                 gt_df=pd.DataFrame, rh_between_0_1=True, verbose=True):
        self.gt_df = gt_df

        self.algo = algo
        self.threshold = threshold

        self.verbose = verbose

        self.rh_between_0_1 = rh_between_0_1
        self.method = optimization_method

        self.pm_column = pm_column

        self.df = df
        self.pm = None
        self.rh = None

        self.params_ranges = {}
        self.params_names = None
        self.params_combinations = None

        self.best_params = None
        self.min_optimization_value = None
        self.optimization_values = None
        self.optimization_values_params = None

        if file_path is not None and df is None:
            self.load_data(file_path)

        self.set_pm_and_rh(rh_column, pm_column)

    def load_data(self, file_path):
        self.df = pd.read_csv(file_path, index_col=0, parse_dates=True)

    def set_pm_and_rh(self, rh_column, pm_column):
        self.pm = self.df[pm_column]
        self.rh = self.df[rh_column]

        self.update_rh_range()

    def update_rh_range(self):
        if max(self.rh) > 1 and self.rh_between_0_1:
            self.rh = self.rh / 100

    def set_params_ranges(self, params_names=None, params_ranges=None):
        self.params_names = params_names
        for param_name in params_names:
            self.params_ranges[param_name] = params_ranges[param_name]

        self.set_params_combinations()

    def set_params_combinations(self):
        self.params_combinations = list(itertools.product(*self.params_ranges.values()))

    def get_params_by_key(self, key):
        return dict(zip(self.params_ranges.keys(), key))

    def get_pm_corrected(self, key):
        params = self.get_params_by_key(key)

        gf = GrowthFunctionFactory(params).get_growth_function(self.algo)
        return self.pm / gf(self.rh)

    def get_correlation(self, pm_corrected):
        return abs(pm_corrected.corr(self.rh))

    def get_training_dtw(self, pm_corrected):
        pm_corrected = pm_corrected.resample('1h').mean()
        pm_corrected.dropna(inplace=True)
        gt_hourly = self.gt_df.resample('1h').mean()
        gt_hourly.dropna(inplace=True)
        index_intersection = pm_corrected.index.intersection(gt_hourly.index)

        return dtw(pm_corrected[index_intersection], gt_hourly.loc[index_intersection])

    def get_training_dist_correlation(self, pm_corrected):
        pm_under_threshold = self.pm[self.rh[self.rh <= self.threshold].index]
        pm_corrected_over_threshold = pm_corrected[self.rh[self.rh > self.threshold].index]

        mean_pm_under_threshold = pm_under_threshold.mean()
        mean_pm_over_threshold = pm_corrected_over_threshold.mean()

        sd_pm_under_threshold = pm_under_threshold.std()
        sd_pm_over_threshold = pm_corrected_over_threshold.std()

        return abs(mean_pm_under_threshold - mean_pm_over_threshold) / math.sqrt(
            sd_pm_under_threshold ** 2 + sd_pm_over_threshold ** 2)

    def get_metric(self):
        if self.method == 'min-correlation':
            return self.get_correlation
        elif self.method == 'dtw':
            return self.get_training_dtw
        elif self.method == 'dist-correlation':
            return self.get_training_dist_correlation

    def save_results_to_file(self, path_to_save, values_to_save=None):
        print('>>> Saving results to file...')
        with open(path_to_save, 'a', newline='', encoding='utf-8') as f_object:
            writer_object = writer(f_object)
            writer_object.writerow(
                values_to_save + [self.pm_column, self.algo, self.best_params, self.min_optimization_value,
                                  self.method])
            f_object.close()

    def optimize(self):
        if self.verbose:
            print('>>> Optimizing ', self.algo, ' parameters through ', self.method)

        values = []
        values_params = {}

        for param_combination in self.params_combinations:
            pm_corrected = self.get_pm_corrected(param_combination)
            optimization_value = self.get_metric()(pm_corrected)

            values_params[optimization_value] = param_combination
            values.append(optimization_value)

        self.min_optimization_value = min(values)
        self.set_best_params(values_params)

        self.optimization_values = values
        self.optimization_values_params = values_params

    def set_best_params(self, resulting_params):
        best_params = resulting_params[min(resulting_params.keys())]
        self.best_params = dict(zip(self.params_names, best_params))

    def get_best_params(self):
        return self.best_params

    def get_figure_of_merit(self, file_name):
        fom = FigureOfMerit(self.optimization_values, self.params_combinations, params_names=self.params_names)
        return fom.plot_fom(title=file_name + ' - ' + str(self.best_params))
