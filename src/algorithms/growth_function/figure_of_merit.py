# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 10:28:19 2022

@author: marti
"""
import matplotlib.pyplot as plt


class FigureOfMerit:
    def __init__(self, values, params_combination, params_names):
        self.values = values
        self.params_combinations = params_combination
        self.params_names = params_names

    def get_formatted_params_combination(self):
        params_combinations_formatted = []
        for p in self.params_combinations:
            p = list(p)
            for i in range(len(p)):
                p[i] = f'{self.params_names[i]}: {str(round(p[i], 3))}'
            params_combinations_formatted.append(' '.join(p))

        return params_combinations_formatted

    def plot_fom(self, title='FOM', xlabel='Params combinations', ylabel='Optimization values'):
        plt.figure(figsize=(15, 8))

        params_combinations_formatted = self.get_formatted_params_combination()

        plt.title(title)
        plt.plot(params_combinations_formatted, self.values, marker='o')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.xticks(rotation=90, fontsize=9)
        plt.tight_layout()

        return plt
