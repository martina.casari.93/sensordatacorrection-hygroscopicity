
#Hygroscopicity in Low-Cost PM Sensors

This repository contains code and resources related to the problem of hygroscopicity in low-cost particulate matter (PM) sensors. The presence of high humidity levels in the air often leads to overestimation of PM levels by these sensors, compromising the accuracy of air quality measurements. This repository aims to address this challenge and provide solutions for correcting PM measurements in the presence of humidity.

## Contents
- data/: Contains sample data files for testing and experimentation.
- src/: Includes Python scripts and analysis, correction algorithms, and visualization.

## Key Features
- Implementation of corrective functions for addressing the impact of relative humidity on low-cost PM sensors.
- Evaluation and comparison of different correction algorithms using real-world sensor data and reference measurements.
- Visualizations and analysis of PM concentration levels before and after correction.
- 
## Getting Started
1. Clone the repository: git clone https://gitlab.com/martina.casari.93/sensordatacorrection-hygroscopicity.git
2. Install the required dependencies: pip install -r requirements.txt
3. Explore the src/ directory for data analysis and correction algorithms.
4. Use the provided sample data in the data/ directory or replace it with your own dataset.
5. Run the scripts call_procedure.py to perform data analysis, apply correction algorithms, and visualize the results.

## Contributing
Contributions to this repository are welcome. If you have any suggestions, improvements, or new ideas related to hygroscopicity in low-cost PM sensors, please feel free to open an issue or submit a pull request.

## License
This project is licensed under the General Public License v3.0 (GPL-3.0), available from June 2023.
## References

## Acknowledgments
This research has been developed as part of the doctoral program supported by the PON Research and Innovation 14-20: D.M. n. 1061 del 10-8-2021. We would like to thank Wiseair for providing access to low-cost sensor data and collaborating on technical aspects of the research.

For more details and instructions, please refer to the documentation and code files in the repository.